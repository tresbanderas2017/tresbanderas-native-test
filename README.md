# TresBanderas Native Test

## Introducción
Aplicación funcionando con Nativescript y VainillaJS ES6

## Tecnologías
 - Nativescript
 - VainillaJS ES6

## Requisitos
 - Java 8
 - Android SDK

## Instalación de dependencias

```sh
$ tns install babel
$ npm install
```

## Variables globales
```sh
$ export JAVA_HOME=/usr/lib/jvm/jdk-8-oracle-x64
$ export PATH=${PATH}:/home/<USERNAME>/Android/Sdk/platform-tools/:/home/<USERNAME>/Android/Sdk/tools/
$ export ANDROID_HOME=/home/<USERNAME>/Android/Sdk/
```

## Generar APK

### Listado de dispositivos conectados
```sh
$ tns device list
```

### Ejecutar en dispositivo
```sh
$ tns run android --device <NRO>
```
